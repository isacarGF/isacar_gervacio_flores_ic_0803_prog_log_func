public class LambdaTest {
    public static void main(String[] args) {
        Operaciones op = (num1, num2) -> System.out.println(num1 - num2);
        op.imprimeOperacion(25, 30);
    }
}
